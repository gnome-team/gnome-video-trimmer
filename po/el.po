# Greek translation for video-trimmer.
# Copyright (C) 2024 video-trimmer's COPYRIGHT HOLDER
# This file is distributed under the same license as the video-trimmer package.
# Efstathios Iosifidis <eiosifidis@gnome.org>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: video-trimmer master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/YaLTeR/video-trimmer/-/"
"issues\n"
"POT-Creation-Date: 2024-01-02 08:55+0000\n"
"PO-Revision-Date: 2024-01-02 08:55+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Greek <gnome-el-list@gnome.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Can't extract filenames from a VariantDict yet.
#. Translators: --output commandline option description.
#: src/application.rs:37
msgid "Output file path"
msgstr "Διαδρομή αρχείου εξόδου"

#. Translators: --output commandline option arg description.
#: src/application.rs:39
msgid "PATH"
msgstr "ΔΙΑΔΡΟΜΗ"

#: src/main.rs:40 src/window.blp:27
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.desktop.in.in:3
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:5
msgid "Video Trimmer"
msgstr "Επεξεργαστής Βίντεο"

#: src/shortcuts.blp:9
msgctxt "shortcut window"
msgid "General"
msgstr "Γενικά"

#: src/shortcuts.blp:12
msgctxt "shortcut window"
msgid "New window"
msgstr "Νέο παράθυρο"

#: src/shortcuts.blp:17
msgctxt "shortcut window"
msgid "Play / Pause"
msgstr "Αναπαραγωγή / Παύση"

#: src/shortcuts.blp:22
msgctxt "shortcut window"
msgid "Trim"
msgstr "Περικοπή"

#: src/shortcuts.blp:27
msgctxt "shortcut window"
msgid "Set Start at current postion"
msgstr "Ορισμός αρχής στην τρέχουσα θέση"

#: src/shortcuts.blp:32
msgctxt "shortcut window"
msgid "Set End at current position"
msgstr "Ορισμός τέλους στην τρέχουσα θέση"

#: src/shortcuts.blp:37
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Συντομεύσεις πληκτρολογίου"

#: src/shortcuts.blp:42
msgctxt "shortcut window"
msgid "Close window"
msgstr "Κλείσιμο παραθύρου"

#: src/shortcuts.blp:47
msgctxt "shortcut window"
msgid "Quit"
msgstr "Έξοδος"

#. Translators: Title that is shown if we couldn't load the video track for the opened file (but possibly loaded audio).
#: src/video_preview.blp:17
msgid "Could Not Load Video Track"
msgstr "Δεν ήταν δυνατή η φόρτωση του αποσπάσματος του βίντεο"

#. Translators: Description that is shown if we couldn't load the video track for the opened file (but possibly loaded audio).
#: src/video_preview.blp:20
msgid ""
"Maybe the file only has an audio track, or maybe the necessary video codec "
"is missing."
msgstr ""
"Ίσως το αρχείο έχει μόνο κομμάτι ήχου, ή ίσως λείπει ο απαραίτητος κωδικοποιητής βίντεο."

#. Translators: this is appended to the output video file name.
#. So for example "my video.mp4" will become "my video (trimmed).mp4".
#: src/window.rs:300
msgid " (trimmed)"
msgstr " (περικομμένο)"

#. Translators: checkbox in output file selection dialog that strips audio from the
#. video file.
#: src/window.rs:325
msgid "Remove audio"
msgstr "Αφαίρεση ήχου"

#. Translators: checkbox in output file selection dialog.
#: src/window.rs:330
msgid "Accurate trimming, but slower and may lose quality"
msgstr "Ακριβής περικοπή, αλλά πιο αργή και μπορεί να χάσει ποιότητα"

#. Translators: error dialog title.
#: src/window.rs:349 src/window.rs:594 src/window.rs:1010
msgid "Error"
msgstr "Σφάλμα"

#. Translators: error dialog text.
#: src/window.rs:352 src/window.rs:597 src/window.rs:1013
msgid ""
"Video Trimmer can only operate on local files. Please choose another file."
msgstr "Το πρόγραμμα Επεξεργασίας Βίντεο μπορεί να λειτουργήσει μόνο σε τοπικά αρχεία. Παρακαλούμε επιλέξτε ένα άλλο αρχείο."

#. Translators: message dialog text.
#: src/window.rs:454
msgid "Trimming…"
msgstr "Γίνεται Περικοπή…"

#. Translators: text on the toast after trimming was done.
#. The placeholder is the video filename.
#: src/window.rs:478
msgid "{} has been saved"
msgstr "Το {} έχει αποθηκευτεί"

#. Translators: text on the button of the toast after
#. trimming was done to show the output file in the file
#. manager.
#: src/window.rs:485
msgid "Show in Files"
msgstr "Εμφάνιση στα Αρχεία"

#. Translators: error dialog text.
#: src/window.rs:506
msgid "Error trimming video"
msgstr "Σφάλμα κατά την περικοπή του βίντεο"

#: src/window.rs:515
msgid "Could not communicate with the ffmpeg subprocess"
msgstr "Δεν ήταν δυνατή η επικοινωνία με τη διεργασία-υποπρόγραμμα ffmpeg"

#. Translators: error dialog text.
#: src/window.rs:556
msgid "Could not create the ffmpeg subprocess"
msgstr "Αδυναμία δημιουργίας της διεργασίας-υποπρογράμματος ffmpeg"

#. Translators: shown in the About dialog, put your name here.
#: src/window.rs:776
msgid "translator-credits"
msgstr ""
"Ελληνική μεταφραστική ομάδα GNOME\n"
" Ευστάθιος Ιωσηφίδης <iosifidis@opensuse.org>\n"
"\n"
"Για περισσότερες πληροφορίες, επισκεφθείτε τη σελίδα\n"
"http://gnome.gr/"

#. Translators: link title in the About dialog.
#: src/window.rs:779
msgid "Contribute Translations"
msgstr "Συνεισφορά Μεταφράσεων"

#: src/window.rs:787
msgid "This release contains a minor visual refresh for GNOME 45."
msgstr "Αυτή η έκδοση περιλαμβάνει μια μικρή ανανέωση της εμφάνισης για το GNOME 45."

#: src/window.rs:788
msgid "Tweaked the visual style for the GNOME 45 release."
msgstr "Διορθώθηκε το οπτικό στυλ για την έκδοση GNOME 45."

#: src/window.rs:789
msgid "Fixed app crashing when ffprobe is missing."
msgstr "Διορθώθηκε η συντριβή της εφαρμογής όταν λείπει το ffprobe."

#: src/window.rs:790
msgid "Updated to the GNOME 45 platform."
msgstr "Ενημερώθηκε στην πλατφόρμα GNOME 45."

#: src/window.rs:791
msgid "Updated translations."
msgstr "Ενημερώθηκαν οι μεταφράσεις."

#. Translators: file chooser file filter name.
#: src/window.rs:979
msgid "Video files"
msgstr "Αρχεία βίντεο"

#. Translators: file chooser dialog title.
#: src/window.rs:988
msgid "Open video"
msgstr "Άνοιγμα βίντεο"

#. Translators: Primary menu entry that opens a new window.
#: src/window.blp:8
msgid "_New Window"
msgstr "_Νέο Παράθυρο"

#. Translators: Primary menu entry that opens the Keyboard Shortcuts dialog.
#: src/window.blp:14
msgid "_Keyboard Shortcuts"
msgstr "_Συντομεύσεις Πληκτρολογίου"

#. Translators: Primary menu entry that opens the About dialog.
#: src/window.blp:20
msgid "_About Video Trimmer"
msgstr "_Περί με τον Επεξεργαστή Βίντεο"

#. Translators: Main menu button tooltip.
#: src/window.blp:50 src/window.blp:106
msgid "Main Menu"
msgstr "Κυρίως μενού"

#. Translators: Title text on the window when no files are open.
#: src/window.blp:59
msgid "Trim Videos"
msgstr "Περικοπή βίντεο"

#. Translators: Description text on the window when no files are open.
#: src/window.blp:62
msgid "Drag and drop a video here"
msgstr "Σύρετε και αποθέστε ένα βίντεο εδώ"

#. Translators: Open button label when no files are open.
#: src/window.blp:66
msgid "Open Video…"
msgstr "Άνοιγμα βίντεο…"

#. Translators: Open button tooltip.
#: src/window.blp:68
msgid "Select a Video"
msgstr "Επιλέξτε ένα βίντεο"

#. Translators: Trim button.
#: src/window.blp:92
msgid "Trim"
msgstr "Περικοπή"

#. Translators: Trim button tooltip.
#: src/window.blp:95
msgid "Trim Video"
msgstr "Περικοπή Βίντεο"

#. Translators: Title that is shown upon video preview loading error.
#: src/window.blp:132
msgid "Video Preview Failed to Load"
msgstr "Αποτυχία φόρτωσης προεπισκόπησης βίντεο"

#. Translators: Description that is shown upon video preview loading error.
#: src/window.blp:135
msgid ""
"Please enter the start and end timestamps manually.\n"
"\n"
"If you're running Video Trimmer under Flatpak, note that opening files by "
"drag-and-drop may not work."
msgstr ""
"Εισαγάγετε χειροκίνητα τα χρονικά σημεία έναρξης και λήξης.\n"
"\n"
"Αν εκτελείτε τον Επεξεργαστή Βίντεο υπό το Flatpak, σημειώστε ότι το "
"άνοιγμα αρχείων με μεταφορά και απόθεση ενδέχεται να μην λειτουργεί."

#. Translators: Label for the entry for the start timestamp.
#: src/window.blp:155
msgid "Start"
msgstr "Έναρξη"

#. Translators: Label for the entry for the end timestamp.
#: src/window.blp:172
msgid "End"
msgstr "Λήξη"

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.desktop.in.in:4
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:6
msgid "Trim videos quickly"
msgstr "Περικοπή βίντεο γρήγορα"

#. Translators: search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.desktop.in.in:6
msgid "Cut;Movie;Film;Clip;"
msgstr "Αποκοπή;Ταινία;Ταινία;Κλιπ;"

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:12
msgid "Ivan Molodetskikh"
msgstr "Ivan Molodetskikh"

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:18
msgid ""
"Video Trimmer cuts out a fragment of a video given the start and end "
"timestamps. The video is never re-encoded, so the process is very fast and "
"does not reduce the video quality."
msgstr ""
"Ο Επεξεργαστής Βίντεο αφαιρεί ένα απόσπασμα ενός βίντεο δεδομένων των χρονικών "
"σημείων έναρξης και λήξης. Το βίντεο δεν επανακωδικοποιείται ποτέ, έτσι η "
"διαδικασία είναι πολύ γρήγορη και δεν μειώνει την ποιότητα του βίντεο."

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:25
msgid "Main Window"
msgstr "Κυρίως παράθυρο"

